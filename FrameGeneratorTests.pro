QT       += testlib
QT       -= gui

TARGET = tst_framegeneratortests

CONFIG += C++11
CONFIG += console
CONFIG -= app_bundle


TEMPLATE = app

SOURCES += tst_framegeneratortest.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy/

# FrameGenerator
LIBS += -L$$DESTDIR/ -lFrameGenerator

INCLUDEPATH += $$PWD/../FrameGenerator
DEPENDPATH += $$PWD/../FrameGenerator
