#include <QString>
#include <QtTest>

#include <iostream>

#include "framegenerator.h"


void print(uint16_t const * const a, unsigned long int length) {
    for (unsigned long int i = 0; i < length; ++i) {
        std::cout << "pixels[" << i << "] = " << a[i] << std::endl;
    }
    std::cout << std::endl;
}


class FrameGeneratorTest : public QObject
{
    Q_OBJECT

public:
    FrameGeneratorTest();

private Q_SLOTS:
    void SmallestChunkSize_data();
    void SmallestChunkSize();
};

FrameGeneratorTest::FrameGeneratorTest()
{
}

void FrameGeneratorTest::SmallestChunkSize_data()
{
//    QTest::addColumn<QString>("data");
//    QTest::newRow("0") << QString();
}

void FrameGeneratorTest::SmallestChunkSize()
{
    size_t size = 10*1;
    short unsigned int nFrames = 2;
    short unsigned int nUpdatePercentage = 50;
    short unsigned int nUpdateChunkSize = 2;

    FrameGenerator frameGenerator(size, nFrames, nUpdatePercentage, nUpdateChunkSize);
    frameGenerator.generateFrames();

    size_t nPixels = frameGenerator.frameSize();

    QCOMPARE(size, nPixels);

    uint16_t* frame = nullptr;
    while((frame = frameGenerator.read())) {
        print(frame, nPixels);
    }
}

QTEST_APPLESS_MAIN(FrameGeneratorTest)

#include "tst_framegeneratortest.moc"
